import os
import uuid
from enum import Enum
from typing import List

import boto3

SQS_MAX_BATCH_SIZE = 10


class ResourceType(Enum):
    NOTICE = "notice"
    PROCUREMENT = "procurement"


class MissingEnvironmentVariableException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class ScanTableException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class TableNotFoundException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class SqsInsertionException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


def dynamodb_client():
    return boto3.client("dynamodb")


def dynamodb_resource():
    return boto3.resource('dynamodb')


def sqs_client():
    return boto3.client('sqs')


def get_env_var_value(env_variable_name: str) -> str:
    try:
        return os.environ[env_variable_name]
    except KeyError as err:
        raise MissingEnvironmentVariableException(
                f"Missing environment variable '{env_variable_name}', process stopped.") from err


def get_table_resource(table_name: str):
    client = dynamodb_client()
    try:
        client.describe_table(TableName=table_name)
        table = dynamodb_resource().Table(table_name)
        return table
    except client.exceptions.ResourceNotFoundException as err:
        raise TableNotFoundException(f'Table {table_name} does not exist or is being created: {err}')


def extract_resource_id_from_items(items: List) -> List:
    result = list()
    for item in items:
        result.append(item['resource_id'])
    return result


def insert_in_queue(entries: List[dict], queue_url: str) -> None:
    try:
        sqs_client().send_message_batch(QueueUrl=queue_url, Entries=entries)
    except Exception as err:
        raise SqsInsertionException(f"Error while inserting in the SQS queue: {err}") from err


def insert_batches_in_queue(resource_ids: List[str], resource_type: ResourceType) -> None:
    queue_url = get_env_var_value("INGESTION_QUEUE_URL")
    batches = [resource_ids[x:x + SQS_MAX_BATCH_SIZE] for x in range(0, len(resource_ids), SQS_MAX_BATCH_SIZE)]
    print(f'Inserting {len(resource_ids)} {resource_type.value}(s) in SQS queue : {queue_url}')
    for batch in batches:
        entries = []
        for resource_id in batch:
            entry = {'Id': str(uuid.uuid4()),
                     'MessageAttributes': {'type': {'StringValue': resource_type.value, 'DataType': 'String'}},
                     'MessageBody': resource_id}
            entries.append(entry)
        insert_in_queue(entries, queue_url)


def scan_ingestion_tasks_table(resource_type: ResourceType) -> List[str]:
    table_name = get_env_var_value("INGESTION_TASKS_TABLE_NAME")
    resource_ids = list()
    try:
        table = get_table_resource(table_name)
        response = table.scan(ScanFilter={
                'resource_type': {'AttributeValueList': [resource_type.value],
                                  'ComparisonOperator': 'EQ'}})
        resource_ids.extend(extract_resource_id_from_items(response.get("Items", [])))
        while response.get('LastEvaluatedKey'):
            response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'], ScanFilter={
                'resource_type': {'AttributeValueList': [resource_type.value],
                                  'ComparisonOperator': 'EQ'}})
            resource_ids.extend(extract_resource_id_from_items(response.get("Items", [])))
        print(f"{len(resource_ids)} {resource_type.value}s to ingest from DynamoDB")
        return resource_ids
    except TableNotFoundException as err:
        raise err
    except Exception as err:
        raise ScanTableException(f"Scan Table Exception: {err}") from err


resource_type = ResourceType(get_env_var_value('RESOURCE_TYPE'))
print(f'Method ingest_resources triggered with parameters : {resource_type}')
resources_to_process = scan_ingestion_tasks_table(resource_type)
insert_batches_in_queue(resources_to_process, resource_type)
print(f'{len(resources_to_process)} {resource_type.value}(s) added to ingestion queue.')
