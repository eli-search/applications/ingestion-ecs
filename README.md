# Ingestion ECS

This project contains a Python script used during the ingestion of notices in S3. 
This script reads the IDs of the notices to ingest that are stored in S3 and send them to the ingestion SQS Queue in order to be ingested in parallel later during the process.

## Requirements 

* python 3.10
* pip

## Setup

```shell
$ python3 -m venv venv
$ source venv/bin/activate
$ pip3 install -r requirements.txt
```

## Working
This ECS task is triggered automatically by the ingestion Step Function. The python script `app/ingest_resources.py` will be run inside the ECS task.
The ingestion Step Function will wait until the script finishes and continue its progress.
The process is completely automatic, it is not possible to run this locally.

## CI/CD
TED AI AWS environment contains several Amazon ECR repositories:
* ci-temporary-images: for all docker images pushed during CI setup for testing. These images are automatically deleted after 1 day
* ted-applications: for docker images of applications

This pipeline deploys in ci-temporary-images when in a feature branch or main. When a tag is created, the docker image is pushed to ted-applications.
